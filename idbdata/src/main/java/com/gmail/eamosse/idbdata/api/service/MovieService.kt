package com.gmail.eamosse.idbdata.api.service

import com.gmail.eamosse.idbdata.api.response.TokenResponse
import com.gmail.eamosse.imdb.response.CategoryResponse
import retrofit2.Response
import retrofit2.http.GET

internal interface MovieService {
    @GET("authentication/token/new")
    suspend fun getToken(): Response<TokenResponse>

    @GET("/genre/movie/list")
    suspend fun getCategories(): Response<CategoryResponse>
}